package com.ws;

import com.ws.customMgt.model.AccountType;
import com.ws.customMgt.model.BankAccount;
import com.ws.customMgt.repository.BankAccounRepository;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Date;
import java.util.UUID;

@SpringBootApplication
@SecurityScheme(
		name = "keycloak"
		,openIdConnectUrl = "http://localhost:8080/realms/softlearning/.well-known/openid-configuration"
		,scheme = "bearer"
		,type = SecuritySchemeType.OPENIDCONNECT
		,in = SecuritySchemeIn.HEADER
)
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}


	@Bean
	CommandLineRunner start(BankAccounRepository bankAccountRepo){
		return args ->{
			for(int i = 0; i<10; i ++){
				BankAccount bankAct =BankAccount.builder()
						.id(UUID.randomUUID().toString())
						.type(Math.random()>0.5? AccountType.CURRENT_ACCOUNT:AccountType.SAVE_ACCOUNT)
						.balance(10000+Math.random()*90000)
						.currency("XAF")
						.createAt(new Date())
						.build();
				bankAccountRepo.save(bankAct);

			}
		};
	}
}
