package com.ws.customMgt.security.swagger;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.ws.customMgt.security.TokenConverterProperties;

import java.util.List;

@Configuration
@OpenAPIDefinition(servers = {
        @Server(url = "/", description = "Default Server URL")
})
public class OpenAPISecurityConfig {

    @Autowired
    private TokenConverterProperties TokenConverterProperties;

    private static final String OAUTH_SCHEME_NAME = "my_oAuth_security_schema";

    @Value("springdoc-openapi-ui")
    private String serviceTitle;

    @Value("1.6.12")
    private String serviceVersion;

    @Bean
    public OpenAPI customOpenAPI() {
        final String securitySchemeName = "bearerAuth";
        return new OpenAPI()
                .components(
                        new Components().addSecuritySchemes(
                                securitySchemeName,
                                new SecurityScheme()
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .bearerFormat("JWT")
                        )
                )
                .security(List.of(new SecurityRequirement().addList(securitySchemeName)))
                .info(new Info().title("serviceTitle").version(serviceVersion));
}

   /* @Bean
    public OpenAPI openAPI() {
        return new OpenAPI().components(new Components()
                        .addSecuritySchemes(OAUTH_SCHEME_NAME, createOAuthScheme()))
                .addSecurityItem(new SecurityRequirement().addList(OAUTH_SCHEME_NAME))
                .info(new Info().title("Todos Management Service")
                        .description("A service providing todos.")
                        .version("1.0"));
    }

    private SecurityScheme createOAuthScheme() {
        OAuthFlows flows = createOAuthFlows();
        return new SecurityScheme().type(SecurityScheme.Type.OAUTH2)
                .flows(flows);
    }

    private OAuthFlows createOAuthFlows() {
        OAuthFlow flow = createAuthorizationCodeFlow();
        return new OAuthFlows().password(flow);
    }

    private OAuthFlow createAuthorizationCodeFlow() {
        return new OAuthFlow()

                .tokenUrl(TokenConverterProperties.getServerUrl().trim() + "/realms/" + TokenConverterProperties.getRealm().trim() + "/protocol/openid-connect/token")
                .scopes(new Scopes());

               *//* .scopes(new Scopes().addString("read_access", "read data")
                        .addString("write_access", "modify data"));

                *//*
    }

    private SecurityScheme createOpenIdScheme() {
        String connectUrl = TokenConverterProperties.getServerUrl().trim() + "/realms/" + TokenConverterProperties.getRealm().trim() + "/protocol/openid-connect/token";

        return new SecurityScheme()
                .type(SecurityScheme.Type.OPENIDCONNECT)
                .openIdConnectUrl(connectUrl);
    }*/

}