package com.ws.customMgt.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "banckaccount")
public class BankAccount {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "createat")
    private Date createAt;

    @Column(name = "balance")
    private Double balance;

    @Column(name = "currency")
    private String currency;

    @Enumerated(EnumType.STRING)
    @Column(name = "accoutype")
    private AccountType type;

}
