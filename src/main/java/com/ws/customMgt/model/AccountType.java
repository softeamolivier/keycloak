package com.ws.customMgt.model;

public enum AccountType {
    CURRENT_ACCOUNT, SAVE_ACCOUNT
}
