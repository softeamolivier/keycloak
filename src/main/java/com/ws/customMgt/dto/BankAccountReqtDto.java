package com.ws.customMgt.dto;

import com.ws.customMgt.model.AccountType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankAccountReqtDto {
    private Double balance;
    private String currency;
    private AccountType type;
}
