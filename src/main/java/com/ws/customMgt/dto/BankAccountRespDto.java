package com.ws.customMgt.dto;

import com.ws.customMgt.model.AccountType;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BankAccountRespDto {

    @NotNull
    private String id;
    private Date createAt;
    private Double balance;
    private String currency;
    private AccountType type;
}
