package com.ws.customMgt.dto.assembler;

import com.ws.customMgt.controller.AccountController;
import com.ws.customMgt.dto.BankAccountRespDto;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.StreamSupport;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BankAccountRespDtoAssember implements RepresentationModelAssembler<BankAccountRespDto, EntityModel<BankAccountRespDto>> {
    @Override
    public EntityModel<BankAccountRespDto> toModel(BankAccountRespDto item) {

        return EntityModel.of(item,
                linkTo(methodOn(AccountController.class).getBankAccounts(item.getId())).withSelfRel(),
                linkTo(methodOn(AccountController.class).getListBankAccounts()).withRel("bankaccounts"));
    }

    @Override
    public CollectionModel<EntityModel<BankAccountRespDto>> toCollectionModel(Iterable<? extends BankAccountRespDto> items) {
        List<EntityModel<BankAccountRespDto>> entityModelItems = StreamSupport.stream(items.spliterator(), false)
                .map(this::toModel).toList();

        return CollectionModel.of(entityModelItems,
                linkTo(methodOn(AccountController.class).getListBankAccounts()).withSelfRel()
        );
    }
}
