package com.ws.customMgt.service.impl;

import com.ws.customMgt.dto.BankAccountReqtDto;
import com.ws.customMgt.dto.BankAccountRespDto;
import com.ws.customMgt.mappers.AccountMapper;
import com.ws.customMgt.model.BankAccount;
import com.ws.customMgt.repository.BankAccounRepository;
import com.ws.customMgt.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccountService implements IAccountService {

     private BankAccounRepository bankAccounRepo;

    private AccountMapper accountMapper;

    @Autowired
    public AccountService(BankAccounRepository bankAccounRepo, AccountMapper accountMapper){
        this.bankAccounRepo=bankAccounRepo;
        this.accountMapper = accountMapper;
    }


    public List<BankAccountRespDto> bankAccountsList(){
        return bankAccounRepo.findAll().stream().map(v ->  accountMapper.fromBankAccount(v)).collect(Collectors.toList());
    }


    public BankAccountRespDto getbankAccount(String id){
        return accountMapper.fromBankAccount(bankAccounRepo.findById(id)
                .orElseThrow(()->new RuntimeException(String.format("Account %s not found",id))));
    }


    public BankAccountRespDto updatebankAccount(String id, BankAccountReqtDto bankAct){
        BankAccount account = bankAccounRepo.findById(id).orElseThrow();
        if (bankAct.getBalance()!=null)account.setBalance(bankAct.getBalance());
        if (bankAct.getType()!=null)account.setType(bankAct.getType());
        if (bankAct.getCurrency()!=null)account.setCurrency(bankAct.getCurrency());
        account.setCreateAt(new Date());
        return accountMapper.fromBankAccount(bankAccounRepo.save(account));
    }


    public void deletebankAccounts( String id){
        bankAccounRepo.deleteById(id);
    }



    @Override
    public BankAccountRespDto addAccount (BankAccountReqtDto bankActDto){

        BankAccount bankAct =BankAccount.builder()
                .id(UUID.randomUUID().toString())
                .type(bankActDto.getType())
                .balance(bankActDto.getBalance())
                .currency(bankActDto.getCurrency())
                .createAt(new Date())
                .build();
        BankAccount   saveAccount = bankAccounRepo.save(bankAct);

        return accountMapper.fromBankAccount(saveAccount);


    }
}
