package com.ws.customMgt.service;

import com.ws.customMgt.dto.BankAccountReqtDto;
import com.ws.customMgt.dto.BankAccountRespDto;

import java.util.List;

public interface IAccountService {

    BankAccountRespDto addAccount (BankAccountReqtDto bankActDto);
    List<BankAccountRespDto> bankAccountsList();
    BankAccountRespDto getbankAccount(String id);

    BankAccountRespDto updatebankAccount(String id, BankAccountReqtDto bankAct);

    void deletebankAccounts( String id);
}
