package com.ws.customMgt.mappers;

import com.ws.customMgt.dto.BankAccountReqtDto;
import com.ws.customMgt.dto.BankAccountRespDto;
import com.ws.customMgt.model.BankAccount;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {

    public BankAccountRespDto fromBankAccount(BankAccount bankAct){
        BankAccountRespDto result = new BankAccountRespDto();

        BeanUtils.copyProperties(bankAct,result);

        return result ;

    }

    public BankAccount fromBankAccountRequestDto(BankAccountReqtDto bankresq){
        BankAccount result = new BankAccount();
        BeanUtils.copyProperties(bankresq,result);
        return result ;

    }
}
