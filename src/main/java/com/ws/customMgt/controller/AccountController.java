package com.ws.customMgt.controller;

import com.ws.customMgt.dto.BankAccountReqtDto;
import com.ws.customMgt.dto.BankAccountRespDto;
import com.ws.customMgt.dto.assembler.BankAccountRespDtoAssember;
import com.ws.customMgt.repository.BankAccounRepository;
import com.ws.customMgt.service.IAccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@Tag(name = "Accounts", description = "BankAccount OpenApi3 features on ")
@SecurityRequirement(name = "keycloak")
public class AccountController {


    private IAccountService accountService;

    private BankAccountRespDtoAssember bankAccountAssembler;

    @Autowired
    public AccountController(IAccountService accountService,BankAccountRespDtoAssember bankAccountAssembler){
        this.accountService=accountService;
        this.bankAccountAssembler = bankAccountAssembler;
    }



    @Operation(summary = "Get a List of BankAccount", description = " Not Input and it will return the list of all BankAccount", tags = { "BankAccount" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of BankAccount",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = BankAccountRespDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = { @Content(schema = @Schema(implementation = String.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "BankAccount not found",
                    content = { @Content(schema = @Schema(implementation = String.class), mediaType = "application/json") })
               })
    @GetMapping("/bankAccounts")
    public ResponseEntity<CollectionModel<EntityModel<BankAccountRespDto>>> getListBankAccounts(){
        List<BankAccountRespDto> listBankaccount = accountService.bankAccountsList();
        CollectionModel<EntityModel<BankAccountRespDto>> collectionModel = bankAccountAssembler.toCollectionModel(listBankaccount);

        return ResponseEntity.ok()
                .body(collectionModel);
    }



    @Operation(summary = "Get a BankAccount by its id", description = "Input Id and it will return the  BankAccount", tags = { "BankAccount" })
    @Parameter(in= ParameterIn.PATH, description = "BankAccount ID", name = "ID", content = @Content(schema = @Schema(type = "string")))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the BankAccount",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = BankAccountRespDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = { @Content(schema = @Schema(implementation = String.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "BankAccount not found",
                    content = { @Content(schema = @Schema(implementation = String.class), mediaType = "application/json") })
    })
    @GetMapping("/bankAccounts/{id}")
    public ResponseEntity<EntityModel<BankAccountRespDto>>  getBankAccounts(@PathVariable String id) {
        BankAccountRespDto item = accountService.getbankAccount(id);
        if (item == null) {
            return ResponseEntity.notFound().build();
        } else {
            EntityModel<BankAccountRespDto> entityModel = bankAccountAssembler.toModel(item);

            return ResponseEntity.ok()
                    .body(entityModel);
        }
    }


    @Operation(summary = "Add a BankAccount ", description = "Input user BankAccount information and it will return the  BankAccount", tags = { "BankAccount" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the BankAccount",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = BankAccountRespDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = { @Content(schema = @Schema(implementation = String.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "BankAccount not found",
                    content = { @Content(schema = @Schema(implementation = String.class), mediaType = "application/json") })
    })
    @PostMapping("/bankAccounts/")
    public ResponseEntity<EntityModel<BankAccountRespDto>>  createBankAccounts(@RequestBody BankAccountReqtDto bankAct){
        BankAccountRespDto item = accountService.addAccount(bankAct);

        if (item == null) {
            return ResponseEntity.notFound().build();
        } else {
            EntityModel<BankAccountRespDto> entityModel = bankAccountAssembler.toModel(item);

            return ResponseEntity.ok()
                    .body(entityModel);
        }
    }


    @Operation(summary = "Update a BankAccount ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "SUCCESS Update BankAccount",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = BankAccountRespDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Fail to update BankAccount",
                    content = @Content) })
    @PutMapping("/bankAccounts/{id}")
    public ResponseEntity<EntityModel<BankAccountRespDto>>  updateBankAccounts(@PathVariable String id, @RequestBody BankAccountReqtDto bankAct){

        BankAccountRespDto item = accountService.updatebankAccount(id,bankAct);

        EntityModel<BankAccountRespDto> entityModel = bankAccountAssembler.toModel(item);

        return ResponseEntity.ok()
                .body(entityModel);

    }

    @Operation(summary = "Delete a BankAccount ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "SUCCESS Delete BankAccount",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = BankAccountRespDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Fail to delete BankAccount",
                    content = @Content) })
    @DeleteMapping("/bankAccounts/{id}")
    public void deleteBankAccounts(@PathVariable String id){
        accountService.deletebankAccounts(id);
    }

}
