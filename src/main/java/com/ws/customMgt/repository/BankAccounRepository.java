package com.ws.customMgt.repository;

import com.ws.customMgt.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankAccounRepository extends JpaRepository<BankAccount,String> {
}
